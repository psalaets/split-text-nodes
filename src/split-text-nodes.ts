import { findTextNodes } from './find-text-nodes';

export type Options = {
  /**
   * Split a text node.
   *
   * Optional, defaults to splitting by `/\b/`.
   *
   * @param text Text of the text node.
   * @returns Sequence of strings. Every string in this iterable is a candidate
   * to be wrapped in its own element (see `wrap` option).
   */
  split?: (text: string) => Iterable<string>;
  /**
   * Wraps a text chunk yielded by `split()` with an html element.
   *
   * Optional, defaults to wrapping every text chunk with a `<span>`.
   *
   * @param chunk Text chunk to wrap
   * @param textNode Text node that `chunk` came from.
   * @returns Element that wraps `chunk`, or `chunk` itself if `chunk` should
   * not be wrapped.
   */
  wrap?: (chunk: string, textNode: Text) => HTMLElement | string;
};

type SplitResult = {
  /**
   * Wrapper elements created by `wrap`.
   */
  wrappers: Array<HTMLElement>;
  /**
   * Reverts child content of `element` back to its original state.
   */
  revert: () => void;
};

/**
 * Split an element's text nodes into chunks and wrap each chunk.
 *
 * @param element Element containing some text.
 * @param options
 * @returns Wrapper elements that were created and a function that reverts the
 * child content of `element` back to its original state.
 */
export function splitTextNodes(
  element: HTMLElement,
  options: Options = {}
): SplitResult {
  const split: Options['split'] = options.split ?? ((text) => text.split(/\b/));
  const wrap: Options['wrap'] =
    options.wrap ??
    ((chunk) => {
      const el = document.createElement('span');
      el.textContent = chunk;
      return el;
    });

  const wrappers: Array<HTMLElement> = [];

  findTextNodes(element).forEach((textNode) => {
    let hasWrappers = false;

    const replacements = Array.from(split(textNode.data), (chunk) => {
      const wrapResult = wrap(chunk, textNode);

      if (wrapResult instanceof HTMLElement) {
        wrappers.push(wrapResult);
        hasWrappers = true;
      }

      return wrapResult;
    });

    if (hasWrappers) {
      textNode.replaceWith(...replacements);
    }
  });

  return {
    wrappers: wrappers,
    revert() {
      wrappers.forEach((wrapper) =>
        wrapper.replaceWith(wrapper.textContent ?? '')
      );
      element.normalize();
    },
  };
}
