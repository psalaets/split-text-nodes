import { splitTextNodes } from './index';

describe('splitTextNodes', () => {
  test('splits on word boundaries', () => {
    document.body.innerHTML = `
      <div id="el">one two three</div>
    `;

    const { revert, wrappers } = splitTextNodes(document.getElementById('el')!);

    expect(document.body.innerHTML).toMatchSnapshot(
      'text split by word boundaries'
    );
    expect(wrappers).toHaveLength(5);

    revert();

    expect(document.body.innerHTML).toMatchSnapshot(
      'wrapping elements removed'
    );
  });

  test('splits text nodes in children', () => {
    document.body.innerHTML = `
      <div id="el">one <strong>two three</strong> four</div>
    `;

    const { revert, wrappers } = splitTextNodes(document.getElementById('el')!);

    expect(document.body.innerHTML).toMatchSnapshot(
      'text split by word boundaries, at every level of nesting'
    );
    expect(wrappers).toHaveLength(7);

    revert();

    expect(document.body.innerHTML).toMatchSnapshot(
      'wrapping elements removed'
    );
  });

  describe('with custom split', () => {
    test('splits on whatever', () => {
      document.body.innerHTML = `
        <div id="el">one two three</div>
      `;

      const { revert, wrappers } = splitTextNodes(
        document.getElementById('el')!,
        {
          split: (str) => str,
        }
      );

      expect(document.body.innerHTML).toMatchSnapshot(
        'text split by character'
      );
      expect(wrappers).toHaveLength(13);

      revert();

      expect(document.body.innerHTML).toMatchSnapshot(
        'wrapping elements removed'
      );
    });
  });

  describe('with wrap() option', () => {
    test('passes each text chunk and its text node to wrap()', () => {
      document.body.innerHTML = `
        <div id="el">one two three</div>
      `;

      const el = document.getElementById('el')!;
      // Grab the text node *before* `splitTextNodes()` is called because it
      // will be replaced by `splitTextNodes()`.
      const originalTextNode = el.childNodes[0];
      const mockWrap = jest.fn((chunk: string, _textNode: Text) => chunk);

      splitTextNodes(el, {
        wrap: mockWrap,
      });

      expect(mockWrap.mock.calls).toHaveLength(5);

      expect(mockWrap.mock.calls[0][0]).toBe('one');
      expect(mockWrap.mock.calls[0][1]).toBe(originalTextNode);

      expect(mockWrap.mock.calls[1][0]).toBe(' ');
      expect(mockWrap.mock.calls[1][1]).toBe(originalTextNode);

      expect(mockWrap.mock.calls[2][0]).toBe('two');
      expect(mockWrap.mock.calls[2][1]).toBe(originalTextNode);

      expect(mockWrap.mock.calls[3][0]).toBe(' ');
      expect(mockWrap.mock.calls[3][1]).toBe(originalTextNode);

      expect(mockWrap.mock.calls[4][0]).toBe('three');
      expect(mockWrap.mock.calls[4][1]).toBe(originalTextNode);
    });

    test('can exclude some chunks from wrapping', () => {
      document.body.innerHTML = `
        <div id="el">one two three</div>
      `;

      const { revert, wrappers } = splitTextNodes(
        document.getElementById('el')!,
        {
          wrap: (chunk) => {
            if (chunk === 'two') {
              return chunk;
            } else {
              const el = document.createElement('span');
              el.textContent = chunk;
              return el;
            }
          },
        }
      );

      expect(document.body.innerHTML).toMatchSnapshot(
        'text split by word boundaries, not wrapping "two"'
      );
      expect(wrappers).toHaveLength(4);

      revert();

      expect(document.body.innerHTML).toMatchSnapshot(
        'wrapping elements removed'
      );
    });

    test('can exclude all chunks from wrapping', () => {
      document.body.innerHTML = `
        <div id="el">one two three</div>
      `;

      const { revert, wrappers } = splitTextNodes(
        document.getElementById('el')!,
        {
          wrap: (text) => text,
        }
      );

      expect(document.body.innerHTML).toMatchSnapshot(
        'text split by word boundaries, but nothing wrapped'
      );
      expect(wrappers).toHaveLength(0);

      revert();

      expect(document.body.innerHTML).toMatchSnapshot(
        'wrapping elements removed'
      );
    });

    test('different wrapper element', () => {
      document.body.innerHTML = `
        <div id="el">one two three</div>
      `;

      const { revert, wrappers } = splitTextNodes(
        document.getElementById('el')!,
        {
          wrap: (chunk) => {
            const el = document.createElement('mark');
            el.textContent = chunk;
            return el;
          },
        }
      );

      expect(document.body.innerHTML).toMatchSnapshot(
        'text split by word boundaries, with mark wrappers'
      );
      expect(wrappers).toHaveLength(5);

      revert();

      expect(document.body.innerHTML).toMatchSnapshot(
        'wrapping elements removed'
      );
    });

    test('apply custom class to wrapper elements', () => {
      document.body.innerHTML = `
        <div id="el">one two three</div>
      `;

      const { revert, wrappers } = splitTextNodes(
        document.getElementById('el')!,
        {
          wrap: (chunk) => {
            const el = document.createElement('span');
            el.textContent = chunk;
            el.classList.add('c1');
            return el;
          },
        }
      );

      expect(document.body.innerHTML).toMatchSnapshot(
        'custom class on wrappers'
      );
      expect(wrappers).toHaveLength(5);

      revert();

      expect(document.body.innerHTML).toMatchSnapshot(
        'wrapping elements removed'
      );
    });

    test('with multiple additional classes on wrapper elements', () => {
      document.body.innerHTML = `
        <div id="el">one two three</div>
      `;

      const { revert, wrappers } = splitTextNodes(
        document.getElementById('el')!,
        {
          wrap: (chunk) => {
            const el = document.createElement('span');
            el.textContent = chunk;
            el.classList.add('c1', 'c2');
            return el;
          },
        }
      );

      expect(document.body.innerHTML).toMatchSnapshot(
        'all custom classes on wrappers'
      );
      expect(wrappers).toHaveLength(5);

      revert();

      expect(document.body.innerHTML).toMatchSnapshot(
        'wrapping elements removed'
      );
    });

    test('different classes on wrapper elements per chunk', () => {
      document.body.innerHTML = `
        <div id="el">one two three</div>
      `;

      let chunkNumber = 1;
      const { revert, wrappers } = splitTextNodes(
        document.getElementById('el')!,
        {
          wrap: (chunk) => {
            const el = document.createElement('span');
            el.textContent = chunk;
            el.classList.add(`c${chunkNumber++}`);
            return el;
          },
        }
      );

      expect(document.body.innerHTML).toMatchSnapshot(
        'different custom class on each wrapper'
      );
      expect(wrappers).toHaveLength(5);

      revert();

      expect(document.body.innerHTML).toMatchSnapshot(
        'wrapping elements removed'
      );
    });
  });
});
