import { findTextNodes } from './find-text-nodes';

describe('findTextNodes', () => {
  test('returns nothing for empty element', () => {
    document.body.innerHTML = `
      <div id="el"></div>
    `;

    const result = collectText(findTextNodes(document.getElementById('el')!));

    expect(result).toEqual([]);
  });

  test('returns nothing for no text nodes', () => {
    document.body.innerHTML = `
      <div id="el"><hr></div>
    `;

    const result = collectText(findTextNodes(document.getElementById('el')!));

    expect(result).toEqual([]);
  });

  test('returns lone top-level text node', () => {
    document.body.innerHTML = `
      <div id="el">this is text</div>
    `;

    const result = collectText(findTextNodes(document.getElementById('el')!));

    expect(result).toEqual(['this is text']);
  });

  test('returns multiple top-level text nodes', () => {
    document.body.innerHTML = `
      <div id="el">first<br>second</div>
    `;

    const result = collectText(findTextNodes(document.getElementById('el')!));

    expect(result).toEqual(['first', 'second']);
  });

  test('returns nested text node', () => {
    document.body.innerHTML = `
      <div id="el"><span>nested here</span></div>
    `;

    const result = collectText(findTextNodes(document.getElementById('el')!));

    expect(result).toEqual(['nested here']);
  });

  test('returns multi-levels of nested text nodes', () => {
    document.body.innerHTML = `
      <div id="el">level1<span>level2<span>level3</span></span></div>
    `;

    const result = collectText(findTextNodes(document.getElementById('el')!));

    expect(result).toEqual(['level1', 'level2', 'level3']);
  });
});

function collectText(result: ReturnType<typeof findTextNodes>) {
  return [...result].map((text) => text.textContent);
}
