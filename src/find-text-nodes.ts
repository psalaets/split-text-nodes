export function findTextNodes(element: HTMLElement): Array<Text> {
  const walker = document.createTreeWalker(element, NodeFilter.SHOW_TEXT);
  const nodes: Array<Text> = [];

  let node: Node | null = null;
  while ((node = walker.nextNode())) {
    // Can assume it's Text due to filter flags during walker creation
    nodes.push(node as Text);
  }

  return nodes;
}
