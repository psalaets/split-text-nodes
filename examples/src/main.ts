import './style.css';
import 'milligram/dist/milligram.min.css';

import { splitTextNodes } from 'split-text-nodes';

type Result = ReturnType<typeof splitTextNodes>;

function setup(
  exampleId: string,
  splitAction: (container: HTMLElement) => Result
) {
  let result: Result | null = null;

  const container = document.querySelector<HTMLElement>(
    `#${exampleId} .js-text-container`
  )!;

  const splitButton = document.querySelector<HTMLButtonElement>(
    `#${exampleId} .js-split`
  )!;

  const revertButton = document.querySelector<HTMLButtonElement>(
    `#${exampleId} .js-revert`
  )!;

  splitButton.addEventListener('click', () => {
    if (!result) {
      result = splitAction(container);
    }
  });

  revertButton.addEventListener('click', () => {
    if (result) {
      result.revert();
      result = null;
    }
  });
}

setup('default-split', (container) => splitTextNodes(container));

setup('custom-split', (container) =>
  splitTextNodes(container, {
    split: (text) => text,
  })
);

setup('conditional-wrapping', (container) =>
  splitTextNodes(container, {
    wrap: (chunk) => {
      if (chunk.trim().length > 4) {
        const el = document.createElement('span');
        el.textContent = chunk;
        return el;
      } else {
        return chunk;
      }
    },
  })
);

setup('specify-wrapper-tag', (container) =>
  splitTextNodes(container, {
    wrap: (chunk) => {
      if (chunk.trim().length > 4) {
        const el = document.createElement('mark');
        el.textContent = chunk;
        return el;
      } else {
        return chunk;
      }
    },
  })
);

setup('specify-wrapper-classes', (container) =>
  splitTextNodes(container, {
    split: (text) => text,
    wrap: (chunk) => {
      const el = document.createElement('span');
      el.textContent = chunk;

      if ('aeiou'.includes(chunk)) {
        el.classList.add(`vowel--${chunk}`);
      }

      return el;
    },
  })
);
