# Change Log

## [9.0.0] - 2024-04-09

### Breaking

- No longer outputs CommonJS module. Only ES module going forward.

## [8.2.0] - 2024-04-09

### Changed

- Small perf improvement.

## [8.1.0] - 2024-03-15

### Changed

- Use built-in API to find Text nodes.

## [8.0.0] - 2023-06-07

### Breaking

- `splitTextNodes` now returns an object containing the revert function and wrapper elements.

## [7.0.0] - 2023-06-07

### Breaking

- No longer marking wrapper elements with a data attribute (`data-stn-wrapper`).
- Build outputs es2022.

## [6.1.0] - 2023-02-19

### Added

- `Options['wrap']` receives the text node that is being split as its 2nd argument (type: `Text`).
- Export `Options` type.

## [6.0.0] - 2023-02-17

### Breaking

- Wrapper elements are marked with a data attribute (`data-stn-wrapper`) instead of a class.

## [5.0.1] - 2023-02-10

### Fixed

- Docs

## [5.0.0] - 2023-02-10

### Breaking

- `Options['shouldWrap']` no longer exists. Replaced by `Options['wrap']`.
- `Options['wrapperTagName']` no longer exists. Replaced by `Options['wrap']`.
- `Options['additionalWrapperClasses']` no longer exists. Replaced by `Options['wrap']`.
- Chunk wrapper elements are now given the class `stn-wrapper` and are no longer given the class `stn-chunk`.

## [4.0.0] - 2023-02-07

### Breaking

- Normalize text nodes inside the element passed to `splitTextNodes()`.

## [3.1.0] - 2023-01-15

### Added

- Small perf boost when calling `unwrap()`.

## [3.0.0] - 2021-11-19

### Breaking

- No longer compiling to es5.
- No longer supporting IE11.

## [2.2.0] - 2021-11-19

### Changed

- Compile down to es5 to support IE11.

**This is the only release that caters to old browsers.**

## [2.1.0] - 2021-10-25

### Added

- More project metadata

## [2.0.1] - 2021-10-13

### Fixed

- `additionalWrapperClasses` can return an empty string (or empty array) to indicate that no additional classes should be added to wrapper elements. Old behavior used to throw an Error for empty string.

## [2.0.0] - 2021-10-13

### Breaking

- `wrapperClass` is no longer exported. See "Added" section for its replacement.

### Added

- `additionalWrapperClasses` option to apply custom classes to wrapper element.

## [1.4.0] - 2021-10-05

### Added

- New named export `wrapperClass`. Its value is the css class applied to wrapper elements.

## [1.3.1] - 2021-10-05

### Changed

- Output es2019 to support builds that can't handle nullish coalescing operator in 3rd party modules.

## [1.3.0] - 2021-10-05

### Added

- `wrapperTagName` option to control what tag is used to wrap text chunks.

## [1.2.0] - 2021-10-03

### Added

- `shouldWrap` option to enable opting-out text chunks from being wrapped in an element.

## [1.1.0] - 2021-09-25

- Initial release
